import json
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), "lib"))

# Import settings class
from Gary_Settings import GarySettings

#---------------------------------------
# [Required] Script Information
#---------------------------------------
ScriptName = "Gary"
Website = "https://simple.wikipedia.org/wiki/Gary_the_Snail"
Description = "Let's the chat know if Gary has returned"
Creator = "TNTMusicStudios"
Version = "3.0.5"

#---------------------------------------
# Global Variables
#---------------------------------------
DataDir = None
SettingsFile = None
Settings = None
StatsFile = None
Stats = None

#---------------------------
#   [Optional] Reload Settings (Called when a user clicks the Save Settings button in the Chatbot UI)
#---------------------------
def ReloadSettings(jsonData):
    global Settings

    # Execute json reloading here
    Settings.Reload(jsonData)

    return

#---------------------------------------
# [Required] Intialize Data (Only called on Load)
#---------------------------------------
def Init():
    global DataDir
    global SettingsFile
    global Settings
    global StatsFile
    global Stats

    DataDir = os.path.join(os.path.dirname(__file__), "data")
    SettingsFile = os.path.join(os.path.dirname(__file__), "GaryConfig.json")
    Settings = GarySettings(SettingsFile)
    StatsFile = DataDir + '/stats.json'
    Stats = {}

    # Make the data directory if it doesn't exist
    if not os.path.isdir(DataDir):
        os.makedirs(DataDir)

    return

#---------------------------------------
# [Required] Execute Data / Process Messages
#---------------------------------------
def Execute(data):
    global ScriptName
    global Settings

    command = data.GetParam(0).lower()
    if command != "!gary" and command != "!addcookies":
        return

    # Add cookies command
    if command == "!addcookies":
        user_name = data.GetParam(1).translate(None, '@')
        if not user_name:
            send_message("@{0} Usage: !addcookies <user_name> <amount>".format(data.UserName))
        try:
            cookies = int(data.GetParam(2))
        except:
            send_message("@{0} Usage: !addcookies <user_name> <amount>".format(data.UserName))

        # Verify that the user trying to add the cookies is the admin of the !gary command
        if data.UserName != Settings.Admin:
            if data.UserName == "El_Wowom":
                send_message("Nice try, Jerome. WeSmart")

            send_message("{0} was not powerful enough for that command and accidentally removed their own cookies instead. Kappa".format(data.UserName))
            available_cookies = Parent.GetPoints(data.User)
            if cookies > available_cookies:
                Parent.RemovePoints(data.User, data.UserName, available_cookies)
            else:
                Parent.RemovePoints(data.User, data.UserName, cookies)

            return
        else:
            added = Parent.AddPoints(user_name.lower(), user_name, cookies)
            if not added:
                send_message("Failed to add cookies!")
            return

    initialize_stats(data.User, data.UserName)

    # If the user requests stats, retrieve stats
    if data.GetParam(1).lower() == "stats" or data.GetParam(1).lower() == "statistics":
        requested_user = data.GetParam(2).lower().translate(None, '@')
        if requested_user:
            display_user_stats(requested_user)
        else:
            display_global_stats()
        return

    # If the command is on cooldown for the current user let them know
    if Parent.IsOnUserCooldown(ScriptName, "!gary", data.User):
        cooldown_remaining = Parent.GetUserCooldownDuration(ScriptName, "!gary", data.User)
        send_message("@{0} Gary is onto your scent! Please wait {1} minute(s) before rejoining the search.".format(data.UserName, cooldown_remaining/60))
        return

    # Get the number of cookies passed by the user
    try:
        cookies = int(data.GetParam(1))
    except:
        send_message("@{0} Usage: !gary <cookies> | !gary stats [user]".format(data.UserName))
        return

    # Gary must be offered at least one cookie
    if cookies <= 0:
        send_message("@{0} You must offer Gary at least one cookie.".format(data.UserName))
        return

    # Verify the user has enough cookies for their offer
    if cookies > Parent.GetPoints(data.User):
        send_message("@{0} You don't have enough cookies. NotLikeThis".format(data.UserName))
        return
    
    if cookies > Settings.MaxPocketCapacity:
        send_message("@{0} You cannot carry more than {1} cookies at a time.".format(data.UserName, int(Settings.MaxPocketCapacity)))
        return

    # If we get this far, an attempt to find Gary is being made
    Stats[data.User]['attempts'] += 1

    if cookies == 420:
        Parent.RemovePoints(data.User, data.UserName, cookies)
        send_message("{0} got way too baked and ate all 420 cookies... What are we looking for again? peepoFAT".format(data.UserName))

        # Update spent cookie count for user
        Stats[data.User]['eaten'] += cookies
        Stats[data.User]['spent'] += cookies
        write_stats()

        return

    # Generate a random number for pocket capacity (minimum of 1, maximum of MaxPocketCapacity + 50%)
    pocket_capacity = Parent.GetRandom(1, Settings.MaxPocketCapacity + Settings.MaxPocketCapacity*0.5)

    # Drop cookies if you are carrying too much
    if cookies > pocket_capacity:

        # Determine the maximum amount of cookies that can be dropped
        max_drop = round(cookies*(Settings.MaxDropPercent/100.0))

        # Generate a random number to drop (up to the maximum)
        dropped = Parent.GetRandom(1, max_drop)

        Parent.RemovePoints(data.User, data.UserName, dropped)
        send_message("{0} tripped and fell with pockets overflowing with cookies... {1} cookies were dropped. DIESOFCRINGE".format(data.UserName, dropped))

        # Update stats
        Stats[data.User]['dropped'] += dropped
        Stats[data.User]['spent'] += dropped
        write_stats()

        return
    
    # Take the cookies from the user and resume processing
    Parent.RemovePoints(data.User, data.UserName, cookies)

    # Update spent cookie count for user
    Stats[data.User]['spent'] += cookies

    # The search for Gary has begun...
    if cookies == 1:
        send_message("{0} is attempting to lure Gary home with 1 cookie...".format(data.UserName))
    else:
        send_message("{0} is attempting to lure Gary home with {1} cookies...".format(data.UserName, cookies))

    # Determine how many cookies are needed to spot Gary
    spotted_cookies = Parent.GetRandom(1, 100/Settings.SpottedPercentPerCookie)

    # Determine how many cookies are needed to find Gary (or Larry)
    required_cookies = Parent.GetRandom(1, 100/Settings.FoundPercentPerCookie)

    # Check if Gary is spotted
    if cookies >= spotted_cookies:
        
        # Little easter egg
        if cookies >= required_cookies:
            send_message("Congratulations, you found Lary!!! ...Wait, that's not right. modCheck".format(data.UserName))
            
            # Update stats
            Stats[data.User]['larry_spotted'] += 1
            write_stats()

            return
        else:
            send_message("{0} has spotted Gary, but he got away! HYPERAPPLECATRUN".format(data.UserName))

            # Add a cooldown for the current user (Gary is onto their scent)
            if Settings.ScentDuration:
                Parent.AddUserCooldown(ScriptName, "!gary", data.User, Settings.ScentDuration*60)

            # Update stats
            Stats[data.User]['spotted'] += 1
            write_stats()

            return

    # Check if Gary is found
    if cookies >= required_cookies:
        send_message("spongePls Garyspin {0} HAS FOUND GARY!!! Garyspin spongePls".format(data.UserName))
        send_message("!garyfound")
        send_message("As a reward SpongeBob has given {0} {1} VIP tokens!".format(data.UserName, Settings.VIPTokens))
        send_message("!addvip {0} {1}}".format(data.UserName, Settings.VIPTokens))

        # Update stats
        Stats[data.User]['found'] += 1
        write_stats()

        return

    # Gary is still missing!
    send_message("Gary is still missing... Will we ever find him? catJAMCRY")

    # Update stats
    write_stats()

    return

#---------------------------------------
# [Required] Tick Function
#---------------------------------------
def Tick():
    return

# Sends a message in Twitch Chat
def send_message(message):
    Parent.SendTwitchMessage(message)
    return

def initialize_stats(user, user_name):
    global StatsFile
    global Stats

    # Load stats from a file if the file exists
    if os.path.exists(StatsFile):
        with open(StatsFile, 'r') as stats_file:
            Stats = json.load(stats_file)

    # Ensures stats are cleared from previous runs if the data file is removed
    else:
        Stats = {}

    # If the current user is not found, initialize them
    if not user in Stats:

        user_stats = {}
        user_stats['name'] = user_name
        user_stats['attempts'] = 0
        user_stats['spent'] = 0
        user_stats['eaten'] = 0
        user_stats['dropped'] = 0
        user_stats['spotted'] = 0
        user_stats['larry_spotted'] = 0
        user_stats['found'] = 0

        Stats[user] = user_stats

    else:
        if not 'name' in Stats[user]:
            Stats[user]['name'] = user_name
        if not 'attempts' in Stats[user]:
            Stats[user]['attempts'] = 0
        if not 'spent' in Stats[user]:
            Stats[user]['spent'] = 0
        if not 'eaten' in Stats[user]:
            Stats[user]['eaten'] = 0
        if not 'dropped' in Stats[user]:
            Stats[user]['dropped'] = 0
        if not 'spotted' in Stats[user]:
            Stats[user]['spotted'] = 0
        if not 'larry_spotted' in Stats[user]:
            Stats[user]['larry_spotted'] = 0
        if not 'found' in Stats[user]:
            Stats[user]['found'] = 0

    return

def write_stats():
    global StatsFile
    global Stats

    with open(StatsFile, 'w') as stats_file:
        json.dump(Stats, stats_file, indent=4, sort_keys=True)
    
    return

def display_global_stats():
    global Stats

    attempts = 0
    spent = 0
    eaten = 0
    dropped = 0
    spotted = 0
    found = 0

    for user in Stats:
        attempts = attempts + Stats[user]['attempts']
        spent = spent + Stats[user]['spent']
        eaten = eaten + Stats[user]['eaten']
        dropped = dropped + Stats[user]['dropped']
        spotted = spotted + Stats[user]['spotted']
        found = found + Stats[user]['found']

    send_message("The search for Gary continues... Attempts made: {0}, Cookies spent: {1}, Cookies eaten: {2}, Cookies dropped: {3}, Gary spotted: {4} time(s), Gary found: {5} time(s)".format(attempts, spent, eaten, dropped, spotted, found))

    return

def display_user_stats(user):
    global Stats

    if user in Stats:
        name = Stats[user]['name']
        attempts = Stats[user]['attempts']
        spent = Stats[user]['spent']
        eaten = Stats[user]['eaten']
        dropped = Stats[user]['dropped']
        spotted = Stats[user]['spotted']
        found = Stats[user]['found']

        send_message("{0} -- Attempts made: {1}, Cookies spent: {2}, Cookies eaten: {3}, Cookies dropped: {4}, Gary spotted: {5} time(s), Gary found: {6} time(s)".format(name, attempts, spent, eaten, dropped, spotted, found))
    else:
        send_message("No stats found for user \"{0}\".".format(user))

    return