﻿var settings = {
  "MaxPocketCapacity": 1000.0,
  "UserCooldown": 300,
  "MaxDropPercent": 50.0,
  "SpottedPercentPerCookie": 0.02,
  "FoundPercentPerCookie": 0.002,
  "VIPTokens": 5,
  "ScentDuration": 30,
  "Admin": "TNTMusicStudios"
};